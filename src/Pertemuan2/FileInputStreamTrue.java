/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pertemuan2;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author 0011
 */
public class FileInputStreamTrue {

    FileInputStream fis = null;

    
        try {
fis = new FileInputStream("SomeFile");
        DataInputStream dis = new DataInputStream(fis);
        byte[] data = new byte[1024];
        dis.readFully(data);
        String result = new String(data, "UTF-16LE");
    }
    catch (IOException x) {
// Handle error
} finally {
if (fis != null) {
            try {
                fis.close();
            } catch (IOException x) {
// Forward to handler
            }
        }
    }
}
