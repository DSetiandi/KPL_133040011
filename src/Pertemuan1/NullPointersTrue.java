/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pertemuan1;

import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author 0011
 */
public class NullPointersTrue {
    public static int cardinality(Object obj, final Collection col) {   
        int count = 0;   
        if (col == null) {     
            return count;   
        }   
        Iterator it = col.iterator();   
        while (it.hasNext()) {     
            Object elt = it.next();     
            if ((null == obj && null == elt) ||         (null != obj && obj.equals(elt))) {       
                count++;     
            }   
        }   
        return count; 
    } 
    
}
